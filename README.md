# Name
edwords

# Description
A utility to view or edit an encrypted 'words' file

# Requirements
* Bash
* VIM (rvim command)

# Notes
This repository is in three places:
1.  https://github.alaska.edu/UAF-RCS/edwords (private)
2.  https://github.com/rcs-alaska/edwords (public)
3.  https://bitbucket.org/loforbes/edwords/ (public)

Chef pulls from the github.com repository, but all three should be kept 
in sync.

